package com.example.demoplayer;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;

/**
 * Created by ananddubey
 * Dated- 2019-07-17
 */
public class VideoPlayerFragment extends Fragment {

    private PlayerView mVideoPlayer;

    private ExoPlayer player;

    private static String videoUrl = "https://bitmovin-a.akamaihd.net/content/playhouse-vr/m3u8s/105560.m3u8";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video, container, false);
        mVideoPlayer = view.findViewById(R.id.video_player);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        initializePlayer();
    }

    private void initializePlayer() {
        if (mVideoPlayer != null) {
            player = ExoPlayerFactory.newSimpleInstance(getContext(),
                    new DefaultRenderersFactory(getContext()),
                    new DefaultTrackSelector(), new DefaultLoadControl());

            player.setPlayWhenReady(true);
            player.seekTo(0, 0);
            player.prepare(buildMediaSource(Uri.parse(videoUrl)), true, false);

            mVideoPlayer.setPlayer(player);
        }
    }

    private MediaSource buildMediaSource(Uri uri) {
        return new HlsMediaSource.Factory(
                new DefaultHttpDataSourceFactory("exoplayer-codelab")).
                createMediaSource(uri);
    }

    @Override
    public void onStop() {
        player.release();
        super.onStop();
    }
}
