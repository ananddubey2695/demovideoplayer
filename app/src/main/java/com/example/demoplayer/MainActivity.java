package com.example.demoplayer;

import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class MainActivity extends AppCompatActivity {

    FrameLayout mVideoPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mVideoPlayer = findViewById(R.id.video_player_container);
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        VideoPlayerFragment videoPlayerFragment = new VideoPlayerFragment();

        transaction.add(R.id.video_player_container, videoPlayerFragment, "video_frag");

        transaction.commit();
    }
}